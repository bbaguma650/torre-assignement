<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8"/>
  <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
  <meta name="description" content=""/>
  <meta name="author" content=""/>
  <title>Baguma Benjamin Interview</title>
  <!-- loader-->
  <link href="assets/css/pace.min.css" rel="stylesheet"/>
  <script src="assets/js/pace.min.js"></script>
  <!--favicon-->
  <link rel="icon" href="/assets/images/favicon.ico" type="image/x-icon">
  <!-- simplebar CSS-->
  <link href="/assets/plugins/simplebar/css/simplebar.css" rel="stylesheet"/>
  <!-- Bootstrap core CSS-->
  <link href="/assets/css/bootstrap.min.css" rel="stylesheet"/>
  <!-- animate CSS-->
  <link href="/assets/css/animate.css" rel="stylesheet" type="text/css"/>
  <!-- Icons CSS-->
  <link href="/assets/css/icons.css" rel="stylesheet" type="text/css"/>
  <!-- Sidebar CSS-->
  <link href="/assets/css/sidebar-menu.css" rel="stylesheet"/>
  <!-- Custom Style-->
  <link href="/assets/css/app-style.css" rel="stylesheet"/>
  
</head>

<body class="bg-theme bg-theme1">

<!-- start loader -->
   <div id="pageloader-overlay" class="visible incoming"><div class="loader-wrapper-outer"><div class="loader-wrapper-inner" ><div class="loader"></div></div></div></div>
   <!-- end loader -->

<!-- Start wrapper-->
 <div id="wrapper">

  <!--Start sidebar-wrapper-->
   <div id="sidebar-wrapper" data-simplebar="" data-simplebar-auto-hide="true">
     <div class="brand-logo">
   </div>
   <ul class="sidebar-menu do-nicescrol">
      <li class="sidebar-header"></li>
      <li>
        <a href="/">
          <i class="zmdi zmdi-view-dashboard"></i> <span>Profile</span>
        </a>
      </li>
    </ul>
   
   </div>
   <!--End sidebar-wrapper-->

<!--Start topbar header-->
<header class="topbar-nav">
 <nav class="navbar navbar-expand fixed-top">
  <ul class="navbar-nav mr-auto align-items-center">
    <li class="nav-item">
      <a class="nav-link toggle-menu" href="javascript:void();">
       <i class="icon-menu menu-icon"></i>
     </a>
    </li>
    <li class="nav-item">
      <form class="search-bar">
        <input type="text" class="form-control" placeholder="Enter keywords">
         <a href="javascript:void();"><i class="icon-magnifier"></i></a>
      </form>
    </li>
  </ul>
     
  <ul class="navbar-nav align-items-center right-nav-link">
    <li class="nav-item dropdown-lg">

    </li>
    <li class="nav-item dropdown-lg">

    </li>

    <li class="nav-item">

      <ul class="dropdown-menu dropdown-menu-right">
       <li class="dropdown-item user-details">

        </li>

      </ul>
    </li>
  </ul>
</nav>
</header>
<!--End topbar header-->

<div class="clearfix"></div>
    
  <div class="content-wrapper">
    <div class="container-fluid">

      <div class="row mt-3">
        <div class="col-lg-4">
           <div class="card profile-card-2">
            <div class="card-img-block">
                <img class="img-fluid" src="/uploads/barner.png" alt="Card image cap">
            </div>
            <div class="card-body pt-5">
                <img src="/uploads/profilepicture.jpg" alt="profile-image" class="profile">
                <h5 class="card-title">Baguma Benjamin</h5>
                <p class="card-text">Successful Software Engineer offering over 6 years of experience in demanding environments focused on producing cutting-edge systems. Skilled in directing development with creative and performance oriented approach. Well-organized and customer-focused with proven skills in project management and team leadership.</p>
                <div class="icon-block">
                  <a href="javascript:void();"><i class="fa fa-facebook bg-facebook text-white"></i></a>
                  <a href="javascript:void();"> <i class="fa fa-twitter bg-twitter text-white"></i></a>
                  <a href="javascript:void();"> <i class="fa fa-google-plus bg-google-plus text-white"></i></a>
                </div>
            </div>
        </div>

        </div>

        <div class="col-lg-8">
           <div class="card">
            <div class="card-body">
            <ul class="nav nav-tabs nav-tabs-primary top-icon nav-justified">
                <li class="nav-item">
                    <a href="javascript:void();" data-target="#profile" data-toggle="pill" class="nav-link active"><i class="icon-user"></i> <span class="hidden-xs">Profile</span></a>
                </li>
                <li class="nav-item">
                    <a href="javascript:void();" data-target="#messages" data-toggle="pill" class="nav-link"><i class="icon-envelope-open"></i> <span class="hidden-xs">Messages</span></a>
                </li>

            </ul>
            <div class="tab-content p-3">
                <div class="tab-pane active" id="profile">
                    <h5 class="mb-3">Skills and Experience</h5>
                    <div class="row">
                        <div class="col-md-6">
                            <h6>About</h6>
                            <p>
                               Software Developmet, UI/UX Engineer
                            </p>
                            <p>
                               Systems Administrator
                            </p>
                            <h6>Hobbies</h6>
                            <p>
                                Soccer, Swimming, Reading, Travelling.
                            </p>
                        </div>
                        <div class="col-md-6">
                            <h6>Influencer</h6>
                            <a href="skilldetails?id=html" class="badge badge-dark badge-pill">html5</a>
                            <a href="skilldetails?id=php" class="badge badge-dark badge-pill">Php</a>
                            <a href="skilldetails?id=laravel" class="badge badge-dark badge-pill">Laravel</a>
                            <a href="skilldetails?id=angularjs" class="badge badge-dark badge-pill">angularjs</a>
                            <a href="skilldetails?id=css" class="badge badge-dark badge-pill">css3</a>
                            <a href="skilldetails?id=jquery" class="badge badge-dark badge-pill">jquery</a>
                            <a href="skilldetails?id=python" class="badge badge-dark badge-pill">Python</a>
                            <a href="skilldetails?id=c#" class="badge badge-dark badge-pill">C#</a>
                            <hr>
                            <h6>Proficient</h6>
                            <a href="skilldetails?id=c++" class="badge badge-dark badge-pill">C++</a>
                            <a href="skilldetails?id=linux" class="badge badge-dark badge-pill">Linux</a>
                            <a href="skilldetails?id=sysadmin" class="badge badge-dark badge-pill">System Admin</a>
                            <a href="skilldetails?id=server" class="badge badge-dark badge-pill">Servers</a>
                            <a href="skilldetails?id=sql" class="badge badge-dark badge-pill">SQL</a>
                            <a href="skilldetails?id=vb" class="badge badge-dark badge-pill">Visual Basic .NET</a>
                            <a href="skilldetails?id=responsive-design" class="badge badge-dark badge-pill">responsive-design</a>
                            <hr>
                            <h6>Intermediate</h6>
                            <a href="skilldetails?id=scala" class="badge badge-dark badge-pill">Scala</a>

                            <hr>
                            <h6>Novice</h6>
                            <a href="skilldetails?id=swift" class="badge badge-dark badge-pill">Swift</a>
                            <a href="skilldetails?id=ruby" class="badge badge-dark badge-pill">Ruby</a>
                        </div>

                    </div>
                    <!--/row-->
                </div>
                <div class="tab-pane" id="messages">
                  <div class="table-responsive">
                    <table class="table table-hover table-striped">
                        <tbody>                                    
                            <tr>
                                <td>
                                   <span class="float-right font-weight-bold">3 hrs ago</span> Here is your a link to the latest summary report
                                </td>
                            </tr>
                        </tbody> 
                    </table>
                  </div>
                </div>
            </div>
        </div>
      </div>
      </div>
        
    </div>

    <!--start overlay-->
          <div class="overlay toggle-menu"></div>
        <!--end overlay-->
    
    </div>
    <!-- End container-fluid-->
   </div><!--End content-wrapper-->
   <!--Start Back To Top Button-->
    <a href="javaScript:void();" class="back-to-top"><i class="fa fa-angle-double-up"></i> </a>
    <!--End Back To Top Button-->
    
    <!--Start footer-->
    <footer class="footer">
      <div class="container">
        <div class="text-center">
          
        </div>
      </div>
    </footer>
    <!--End footer-->
  
  </div><!--End wrapper-->


  <!-- Bootstrap core JavaScript-->
  <script src="/assets/js/jquery.min.js"></script>
  <script src="/assets/js/popper.min.js"></script>
  <script src="/assets/js/bootstrap.min.js"></script>
    
  <!-- simplebar js -->
  <script src="/assets/plugins/simplebar/js/simplebar.js"></script>
  <!-- sidebar-menu js -->
  <script src="/assets/js/sidebar-menu.js"></script>
  
  <!-- Custom scripts -->
  <script src="/assets/js/app-script.js"></script>
    
</body>
</html>
